class Joueur:
    def __init__(self, nom):
        self.nom = nom

    def __str__(self):
        return self.nom

class Moteur:
    def __init__(self, joueurcourant):
        self.joueurcourant = joueurcourant
    def __str__(self):
        return self.joueurcourant

class Jeton :
    def __init__(self, couleur):
        self.couleur = couleur
    def __str__(self):
        return self.couleur

class Grille:
    def __init__(self, colone, ligne):
        self.colone = colone
        self.ligne = ligne
    def __str__(self):
        return self.colone+self.ligne

joueur_a = Joueur("Marc")
joueur_b = Joueur("Camille")

joueurcourant_a = Moteur("1")

jeton_jaune = Jeton("Jaune")
jeton_rouge = Jeton("rouge")

grille_col = Grille("1", "2")


print(joueur_a)
print(joueurcourant_a)
print(jeton_jaune)
print(grille_col)
